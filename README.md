# prometheus-common: keeping Prometheus packaging DRY

This package aims at providing common resources for other Prometheus packages.

It manages:

* `prometheus` user creation on install
* TODO: `prometheus` user deletion on purge
* creation (on install) and deletion (on purge) of common directories:
  * `/etc/prometheus` (`root:prometheus`)
  * `/var/lib/prometheus` (`prometheus:prometheus`)
  * `/var/log/prometheus` (`prometheus:prometheus`)
* logorotate for all log files in `/var/log/prometheus`
